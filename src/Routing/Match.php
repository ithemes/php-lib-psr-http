<?php
declare(strict_types=1);

namespace iThemes\Lib\PsrHttp\Routing;

final class Match
{
    /** @var ?Route */
    private $route;

    /** @var ?mixed */
    private $error;

    /** @var array */
    private $errorAttributes;

    /** @var array */
    private $urlMatches;

    public const E_NOT_FOUND = 404;
    public const E_METHOD_NOT_ALLOWED = 405;

    private function __construct()
    {
    }

    /**
     * Create a successful match result.
     *
     * @param Route $route
     *
     * @return Match
     */
    public static function asSuccess(Route $route): Match
    {
        $match        = new static();
        $match->route = $route;

        return $match;
    }

    /**
     * Create a failed match result.
     *
     * @param mixed      $error
     * @param Route|null $partialMatch
     * @param array      $attributes
     *
     * @return Match
     */
    public static function asError($error, Route $partialMatch = null, array $attributes = []): Match
    {
        $match                  = new static();
        $match->route           = $partialMatch;
        $match->error           = $error;
        $match->errorAttributes = $attributes;

        return $match;
    }

    /**
     * Whether this match result was successful.
     *
     * @return bool
     */
    public function isSuccess(): bool
    {
        return ! $this->error;
    }

    /**
     * Was a partial match for the route found.
     *
     * @return bool
     */
    public function isPartialMatch(): bool
    {
        return ! $this->isSuccess() && $this->route;
    }

    /**
     * Get the route matched against.
     *
     * This will throw a TypeError at runtime if the match was not successful.
     *
     * @return Route
     */
    public function getRoute(): Route
    {
        return $this->route;
    }

    /**
     * Get the error that occurred when matching.
     *
     * @return mixed
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * Get additional error attributes to pass
     *
     * @return array
     */
    public function getErrorAttributes(): array
    {
        return $this->errorAttributes;
    }

    /**
     * Get pattern matches if the route was a regex.
     *
     * @return array|null
     */
    public function getUrlMatch(): ?array
    {
        return $this->urlMatches;
    }

    /**
     * Set the pattern matches for a regex route.
     *
     * @param array $matches
     *
     * @return Match
     */
    public function withUrlMatch(array $matches): self
    {
        $self             = clone $this;
        $self->urlMatches = $matches;

        return $self;
    }
}
