<?php
declare(strict_types=1);

namespace iThemes\Lib\PsrHttp\Routing;

use iThemes\Lib\PsrHttp\Middleware\Infrastructure\MiddlewareRunnerFactory;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class RouteMiddleware implements MiddlewareInterface
{
    /** @var MiddlewareRunnerFactory */
    private $middlewareRunnerFactory;

    /**
     * RouteMiddleware constructor.
     *
     * @param MiddlewareRunnerFactory $middlewareRunnerFactory
     */
    public function __construct(MiddlewareRunnerFactory $middlewareRunnerFactory)
    {
        $this->middlewareRunnerFactory = $middlewareRunnerFactory;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $route = $request->getAttribute(RoutingMiddleware::ATTR_ROUTE);

        if (! $route instanceof Route || ! $middleware = $route->getMiddleware()) {
            return $handler->handle($request);
        }

        return $this->middlewareRunnerFactory->make($handler, $middleware)->handle($request);
    }
}
