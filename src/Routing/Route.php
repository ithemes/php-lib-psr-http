<?php
declare(strict_types=1);

namespace iThemes\Lib\PsrHttp\Routing;

use iThemes\Lib\PsrHttp\Middleware\Infrastructure\MiddlewareProvider;

interface Route extends MiddlewareProvider
{
    /**
     * Get the unique name identifying this route.
     *
     * @return string
     */
    public function getName(): string;

    /**
     * Get the controller to handle this route.
     *
     * @return string
     */
    public function getController(): string;

    /**
     * Get a list of the HTTP methods allowed.
     *
     * @return string[]
     */
    public function getAllows(): array;

    /**
     * Get extra route configuration used by different parts of the application.
     *
     * For instance, a "schema" extra that could provide a JSON Schema document to validate
     * the request against in a MiddlewareInterface.
     *
     * @return array
     */
    public function getExtras(): array;
}
