<?php
declare(strict_types=1);

namespace iThemes\Lib\PsrHttp\Routing;

use iThemes\Lib\PsrHttp\HttpResponseError;
use Psr\Http\{Message\ResponseFactoryInterface,
    Message\ResponseInterface,
    Message\ServerRequestInterface,
    Message\StreamFactoryInterface,
    Server\MiddlewareInterface,
    Server\RequestHandlerInterface};

class RoutingMiddleware implements MiddlewareInterface
{
    public const ATTR_ROUTE = 'route';
    public const ATTR_URL_MATCH = 'url-match';

    /** @var Router */
    private $router;

    /** @var ResponseFactoryInterface */
    private $responseFactory;

    /** @var StreamFactoryInterface */
    private $streamFactory;

    /**
     * RoutingMiddleware constructor.
     *
     * @param Router                   $router
     * @param ResponseFactoryInterface $responseFactory
     * @param StreamFactoryInterface   $streamFactory
     */
    public function __construct(
        Router $router,
        ResponseFactoryInterface $responseFactory,
        StreamFactoryInterface $streamFactory
    ) {
        $this->router          = $router;
        $this->responseFactory = $responseFactory;
        $this->streamFactory   = $streamFactory;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $match = $this->router->match($request);

        if ($match->isSuccess()) {
            $request = $request->withAttribute(self::ATTR_ROUTE, $match->getRoute());

            if ($match->getUrlMatch() !== null) {
                $request = $request->withAttribute(self::ATTR_URL_MATCH, $match->getUrlMatch());
            }

            return $handler->handle($request);
        }

        switch ($match->getError()) {
            case Match::E_NOT_FOUND:
                return $this->responseFactory->createResponse(404)
                    ->withAddedHeader('Content-Type', 'application/json')
                    ->withBody($this->streamFactory->createStream(json_encode([
                        'code'    => 'not_found',
                        'message' => 'Not Found.'
                    ])));
            case Match::E_METHOD_NOT_ALLOWED:
                $allowed = $match->isPartialMatch() ? $match->getRoute()->getAllows() : [];

                return $this->responseFactory->createResponse(405)
                    ->withAddedHeader('Content-Type', 'application/json')
                    ->withAddedHeader('Allow', implode(', ', $allowed))
                    ->withBody($this->streamFactory->createStream(json_encode([
                        'code'    => 'method_not_allowed',
                        'message' => 'Method not allowed.'
                    ])));
            default:
                throw new \UnexpectedValueException('Unknown match error.');
        }
    }
}
