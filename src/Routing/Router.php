<?php
declare(strict_types=1);

namespace iThemes\Lib\PsrHttp\Routing;

use Psr\Http\Message\ServerRequestInterface;

interface Router
{
    /**
     * Find the matching route for a request.
     *
     * @param ServerRequestInterface $request
     *
     * @return Match
     */
    public function match(ServerRequestInterface $request): Match;
}
