<?php
declare(strict_types=1);

namespace iThemes\Lib\PsrHttp\Routing;

interface URIGenerator
{
    /**
     * Looks up a route by name, and interpolates data to return a URI path.
     *
     * @param string $name      The route name to look up.
     * @param array  $tokenData The data to interpolate into the URI; data keys
     *                          map to attribute tokens in the path.
     *
     * @return string A URI path string.
     */
    public function generate($name, array $tokenData = []): string;
}
