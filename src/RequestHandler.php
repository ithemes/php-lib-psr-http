<?php
declare(strict_types=1);

namespace iThemes\Lib\PsrHttp;

use iThemes\Lib\PsrHttp\Infrastructure\CallableRequestHandler;
use iThemes\Lib\PsrHttp\Middleware\Infrastructure\MiddlewareProvider;
use iThemes\Lib\PsrHttp\Middleware\Infrastructure\MiddlewareRunnerFactory;
use iThemes\Lib\PsrHttp\Routing\Route;
use iThemes\Lib\PsrHttp\Routing\RoutingMiddleware;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class RequestHandler implements RequestHandlerInterface
{
    /** @var MiddlewareRunnerFactory */
    private $runnerFactory;

    /** @var ContainerInterface */
    private $container;

    /**  @var string */
    private $controllersNamespace;

    /**
     * RouteHandler constructor.
     *
     * @param MiddlewareRunnerFactory $runnerFactory
     * @param ContainerInterface      $container
     * @param string                  $controllersNs Optionally specify the base namespace for controllers. This allows
     *                                               a controller definition using just the classname. The namespace
     *                                               should have a trailing slash.
     */
    public function __construct(
        MiddlewareRunnerFactory $runnerFactory,
        ContainerInterface $container,
        string $controllersNs = ''
    ) {
        $this->runnerFactory        = $runnerFactory;
        $this->container            = $container;
        $this->controllersNamespace = $controllersNs;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $route = $request->getAttribute(RoutingMiddleware::ATTR_ROUTE);

        if (! $route instanceof Route) {
            throw new \LogicException('[RouteHandler] Route not set for request.');
        }

        [ $class, $method ] = $this->getClassAndMethod($route->getController());

        $instance = $this->container->get($class);

        if ($instance instanceof MiddlewareProvider) {
            return $this->runnerFactory
                ->make(
                    new CallableRequestHandler($this->getCallable($instance, $class, $method)),
                    $instance->getMiddleware()
                )->handle($request);
        }

        return $this->getCallable($instance, $class, $method)($request);
    }

    /**
     * Get the callable from the controller definition.
     *
     * @param object $instance
     * @param string $class
     * @param string $method
     *
     * @return callable
     */
    private function getCallable(object $instance, string $class, ?string $method): callable
    {
        if ($method) {
            if (is_callable([ $instance, $method ])) {
                return [ $instance, $method ];
            }

            throw new \BadMethodCallException(sprintf(
                '[RouteHandler] Cannot find method %s on controller class %s.',
                $method,
                $class
            ));
        }

        if ($instance instanceof RequestHandlerInterface) {
            return [ $instance, 'handle' ];
        }

        if (is_callable($instance)) {
            return $instance;
        }

        throw new \BadMethodCallException('[RouteHandler] Cannot find callable.');
    }

    /**
     * Parse out a class and method from a controller definition.
     *
     * @param string $controller
     *
     * @return array
     */
    private function getClassAndMethod(string $controller): array
    {
        if (strpos($controller, '@') !== false) {
            [ $class, $method ] = explode('@', $controller, 2);
        } else {
            $class  = $controller;
            $method = null;
        }

        if (! class_exists($class)) {
            $class = $this->controllersNamespace . $class;
        }

        if (! class_exists($class)) {
            throw new \BadMethodCallException(
                sprintf('[RouteHandler] Cannot find corresponding controller class for %s.', $controller)
            );
        }

        return [ $class, $method ];
    }
}
