<?php
declare(strict_types=1);

namespace iThemes\Lib\PsrHttp\Implementation\Aura;

use Aura\Router\Exception\RouteNotFound;
use Aura\Router\Generator;
use iThemes\Lib\PsrHttp\Routing\URIGenerator;

class AuraURIGenerator implements URIGenerator
{
    /** @var Generator */
    private $generator;

    /**
     * AuraURIGenerator constructor.
     *
     * @param Generator $generator
     */
    public function __construct(Generator $generator)
    {
        $this->generator = $generator;
    }

    public function generate($name, array $tokenData = []): string
    {
        try {
            return $this->generator->generate($name, $tokenData);
        } catch (RouteNotFound $e) {
            throw new \LogicException('Could not find route.', 0, $e);
        }
    }
}
