<?php
declare(strict_types=1);

namespace iThemes\Lib\PsrHttp\Implementation\Aura;

use Aura\Router\RouterContainer;
use Aura\Router\Rule\Allows;
use iThemes\Lib\PsrHttp\Routing\Match;
use iThemes\Lib\PsrHttp\Routing\Route;
use iThemes\Lib\PsrHttp\Routing\Router;
use Psr\Http\Message\ServerRequestInterface;

class AuraRouter implements Router
{
    /** @var RouterContainer */
    private $routerContainer;

    /**
     * Router constructor.
     *
     * @param RouterContainer $routerContainer
     */
    public function __construct(RouterContainer $routerContainer)
    {
        $this->routerContainer = $routerContainer;
    }

    public function match(ServerRequestInterface $request): Match
    {
        $matcher = $this->routerContainer->getMatcher();

        $route = $matcher->match($request);

        if ($route instanceof Route) {
            $match = Match::asSuccess($route);

            if ($route->attributes) {
                $match = $match->withUrlMatch($route->attributes);
            }

            return $match;
        }

        $failedRoute = $matcher->getFailedRoute();

        switch ($failedRoute->failedRule) {
            case Allows::class:
                return Match::asError(Match::E_METHOD_NOT_ALLOWED, $failedRoute instanceof Route ? $failedRoute : null);
            default:
                return Match::asError(Match::E_NOT_FOUND);
        }
    }
}
