<?php
declare(strict_types=1);

namespace iThemes\Lib\PsrHttp\Implementation\Aura;

use Aura\Router\Map;
use Psr\Http\Server\MiddlewareInterface;

/**
 * Class AuraMap
 *
 * @package iThemes\Security\SiteScanner
 *
 * @method self accepts(string|array $accepts)
 * @method self addMiddleware(string|MiddlewareInterface ...$middleware)
 * @method self allows(string|array $allows)
 * @method self attributes(array $attributes)
 * @method self auth($auth)
 * @method self defaults(array $defaults)
 * @method self extras(array $extras)
 * @method self handler($handler)
 * @method self host(string $host)
 * @method self secure(bool $secure)
 * @method self special(callable $special)
 * @method self tokens(array $tokens)
 * @method self wildcard(string $wildcard)
 *
 * @method AuraRoute get($name, $path, $handler = null)
 * @method AuraRoute delete($name, $path, $handler = null)
 * @method AuraRoute head($name, $path, $handler = null)
 * @method AuraRoute options($name, $path, $handler = null)
 * @method AuraRoute patch($name, $path, $handler = null)
 * @method AuraRoute post($name, $path, $handler = null)
 * @method AuraRoute put($name, $path, $handler = null)
 */
class AuraMap extends Map
{

}
