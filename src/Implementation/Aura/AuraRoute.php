<?php
declare(strict_types=1);

namespace iThemes\Lib\PsrHttp\Implementation\Aura;

use Aura\Router\Route;
use Psr\Http\Server\MiddlewareInterface;

class AuraRoute extends Route implements \iThemes\Lib\PsrHttp\Routing\Route
{
    /** @var string[] */
    private $middleware = [];

    /**
     * Add a middleware that applies to this specific route.
     *
     * @param string|MiddlewareInterface ...$classes
     *
     * @return AuraRoute
     */
    public function addMiddleware(...$classes): self
    {
        foreach ($classes as $class) {
            if ($class instanceof MiddlewareInterface) {
                $this->middleware[] = $class;
            } elseif (is_string($class) && is_subclass_of($class, MiddlewareInterface::class)) {
                $this->middleware[] = $class;
            } else {
                throw new \InvalidArgumentException(
                    sprintf(
                        'Middleware class must implement MiddlewareInterface, %s given.',
                        is_object($class) ? get_class($class) : \gettype($class)
                    )
                );
            }
        }

        return $this;
    }

    /**
     * Remove a middleware from applying to this route.
     *
     * @param string $class
     *
     * @return AuraRoute
     */
    public function withoutMiddleware(string $class): self
    {
        if (! is_subclass_of($class, MiddlewareInterface::class)) {
            throw new \InvalidArgumentException('Middleware class must implement MiddlewareInterface');
        }

        if (($i = array_search($class, $this->middleware)) !== false) {
            unset($this->middleware[ $i ]);
        }

        return $this;
    }

    /**
     * Get all middleware class names to apply to this route.
     *
     * @return string|MiddlewareInterface[]
     */
    public function getMiddleware(): array
    {
        return $this->middleware;
    }

    public function getController(): string
    {
        return $this->handler;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getAllows(): array
    {
        return $this->allows;
    }

    public function getExtras(): array
    {
        return $this->extras;
    }
}
