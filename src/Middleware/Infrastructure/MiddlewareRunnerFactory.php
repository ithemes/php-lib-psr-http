<?php
declare(strict_types=1);

namespace iThemes\Lib\PsrHttp\Middleware\Infrastructure;

use Psr\Container\ContainerInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class MiddlewareRunnerFactory
{
    /** @var ContainerInterface */
    private $container;

    /**
     * MiddlewareRunnerFactory constructor.
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Make a middleware runner instance.
     *
     * @param RequestHandlerInterface        $terminator
     * @param string[]|MiddlewareInterface[] $middleware
     *
     * @return RequestHandlerInterface
     */
    public function make(RequestHandlerInterface $terminator, array $middleware): RequestHandlerInterface
    {
        return new MiddlewareRunner($this->container, $terminator, $middleware);
    }
}
