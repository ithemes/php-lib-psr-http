<?php
declare(strict_types=1);

namespace iThemes\Lib\PsrHttp\Middleware\Infrastructure;

use Psr\Http\Server\MiddlewareInterface;

interface MiddlewareProvider
{
    /**
     * Get all middleware provided by this object.
     *
     * Can return a mix of MiddlewareInterface instances or string class names.
     *
     * @return iterable|string[]|MiddlewareInterface[]
     */
    public function getMiddleware(): iterable;
}
