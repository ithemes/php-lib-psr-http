<?php
declare(strict_types=1);

namespace iThemes\Lib\PsrHttp\Middleware\Infrastructure;

use Psr\Container\ContainerInterface;
use Psr\Http\{Message\ResponseInterface,
    Message\ServerRequestInterface,
    Server\MiddlewareInterface,
    Server\RequestHandlerInterface};

class MiddlewareRunner implements RequestHandlerInterface
{
    /** @var ContainerInterface */
    private $container;

    /** @var MiddlewareInterface[]|string[] */
    private $middleware;

    /** @var RequestHandlerInterface */
    private $terminator;

    /**
     * Dispatcher constructor.
     *
     * @param ContainerInterface      $container
     * @param RequestHandlerInterface $terminator
     * @param string[]                $middleware Array of middleware class names or instances.
     */
    public function __construct(ContainerInterface $container, RequestHandlerInterface $terminator, array $middleware)
    {
        $this->container  = $container;
        $this->terminator = $terminator;
        $this->middleware = $middleware;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        return $this->frame(0)->handle($request);
    }

    private function frame(int $i): RequestHandlerInterface
    {
        if (! isset($this->middleware[ $i ])) {
            return $this->terminator;
        }

        $middleware = $this->middleware[ $i ];

        if (! $middleware instanceof MiddlewareInterface) {
            $middleware = $this->container->get($middleware);
        }

        return new DispatchFrame($middleware, function () use ($i) {
            return $this->frame($i + 1);
        });
    }
}
