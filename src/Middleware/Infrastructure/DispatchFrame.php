<?php
declare(strict_types=1);

namespace iThemes\Lib\PsrHttp\Middleware\Infrastructure;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class DispatchFrame implements RequestHandlerInterface
{
    /** @var MiddlewareInterface */
    private $middleware;

    /** @var callable */
    private $nextFrame;

    /**
     * DispatchFrame constructor.
     *
     * @param MiddlewareInterface $middleware
     * @param callable            $nextFrame
     */
    public function __construct(MiddlewareInterface $middleware, callable $nextFrame)
    {
        $this->middleware = $middleware;
        $this->nextFrame  = $nextFrame;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $nextFrame = $this->nextFrame;

        return $this->middleware->process($request, $nextFrame());
    }
}
