<?php
declare(strict_types=1);

namespace iThemes\Lib\PsrHttp\Infrastructure;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class CallableRequestHandler implements RequestHandlerInterface
{
    /** @var callable */
    private $callable;

    /**
     * CallableRequestHandler constructor.
     *
     * @param callable $callable Callback that takes a ServerRequest and returns a Response.
     */
    public function __construct(callable $callable)
    {
        $this->callable = $callable;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $cb = $this->callable;

        return $cb($request);
    }
}
